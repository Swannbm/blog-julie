import base64
from io import BytesIO
import qrcode

from django.urls import reverse
from django.views.generic import ListView, DetailView, TemplateView


from .models import BlogPost


class BreadCrumbMixin:
    # Breadcrumbs must containing a dict of title => url_name
    # url_name will be used in django's reverse() function
    # example :
    # breadcrumbs = {
    #     "billets": "blog_post:list",
    #     "les recettes": "blog_post:tag-recette",
    # }
    breadcrumbs = dict()

    def get_context_breadcrumbs(self, pre_crumbs=None, next_crumbs=None):
        crumbs = [
            {"href": reverse("blog_post:list"), "title": "Accueil"},
        ]
        try:
            for title, url_name in self.breadcrumbs.items():
                crumbs.append({"title": title, "href": reverse(url_name)})
        except AttributeError:
            pass
        return crumbs

    def get_context_data(self, **kwargs):
        """Add bredcrumbs to context"""
        breadcrumbs = self.get_context_breadcrumbs()
        breadcrumbs[-1]["is_active"] = True
        kwargs.update({"breadcrumbs": breadcrumbs})
        return super().get_context_data(**kwargs)


class BlogPostListView(BreadCrumbMixin, ListView):
    template_name = "blog/list.html"
    context_object_name = "blog_posts"
    queryset = BlogPost.objects.filter(is_published=True).order_by("-created_date")


class BlogPostDetailView(BreadCrumbMixin, DetailView):
    template_name = "blog/detail.html"
    context_object_name = "blog_post"
    queryset = BlogPost.objects.filter(is_published=True)

    def get_context_breadcrumbs(self):
        crumbs = super().get_context_breadcrumbs()
        crumbs += [
            {
                "href": reverse("blog_post:detail", args={"slug": self.object.slug}),
                "title": self.object.title
            }
        ]
        return crumbs


def url_to_img_b64(url):
    image = qrcode.make(url)
    buffered = BytesIO()
    image.save(buffered, format="JPEG")
    b64 = base64.b64encode(buffered.getvalue())
    return f"data:image/jpeg;base64,{b64.decode()}"


class QRCodeView(BreadCrumbMixin, TemplateView):
    template_name = "blog/qrcode.html"

    def get_context_breadcrumbs(self):
        crumbs = super().get_context_breadcrumbs()
        crumbs += [
            {
                "href": reverse("blog_post:qrcode"),
                "title": "QR code pour connexion mobile"
            }
        ]
        return crumbs

    def get_context_data(self, **kwargs):
        url = "https://blog-de-kelly.osc-fr1.scalingo.io"
        url_admin = url + "/admin"
        kwargs["qrcode_site"] = url_to_img_b64(url)
        kwargs["qrcode_admin"] = url_to_img_b64(url_admin)
        return super().get_context_data(**kwargs)
